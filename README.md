# solv-config

Configuration files

## Recomendations

We recomend not to use the same git repo on all environments, PRE-production an PROduction should be stored on another git repository


## Structure

### Global

Each folder identifies an environment.

Special folders:

 * *local*: used by testing purposes and local configuration
 * *develop* : development environment
 
### In folder structure

The formats accepted by Config-Server are yml and properties. 
But we recomend to use a single file on yml format.


 * *application.yml* common configuration between services: used by architecture
 * *{microservice}.yml* configuration of microservice called {microservice} 
 


Example:

The configuration of the microservice called front-loans will try to find front-loans.yml and if the application is started with an extra profile it will try to find front-loans-profile.yml
